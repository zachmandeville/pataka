# Pataka Changelog

## v2.0.1

New Features
- added version tag in the bottom corner

## v2.0.0

Extracted this from the ahau (whakapapa-ora repo)

New Features
- added auto-updater


